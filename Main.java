package com.company;

import java.time.format.DateTimeFormatter;
import java.util.*;
import java.time.LocalDate;

public class Main {


    public static void calcoloTempoEsecuzioneProgramma()
    {
        long tempoInizio = System.nanoTime();
        long tempoFine   = System.nanoTime();
        long tempoTotale = (tempoFine - tempoInizio)/100;
        System.out.println("Tempo esecuzione programma: " + tempoTotale + " secondi");

    }


    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        DateTimeFormatter italian = DateTimeFormatter.ofPattern("dd/MM/yyyy", Locale.ITALIAN);
        DateTimeFormatter english = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.ENGLISH);
        DateTimeFormatter french = DateTimeFormatter.ofPattern("MM/dd/yyyy", Locale.FRENCH);
        DateTimeFormatter japanese = DateTimeFormatter.ofPattern("yyyy/MM/dd", Locale.JAPANESE);

        System.out.println("INSERIRE PRIMA DATA");

        System.out.println("Inserire anno: ");
        int yy = in.nextInt();
        System.out.println("Inserire mese: ");
        int mm = in.nextInt();
        System.out.println("Inseire giorno: ");
        int gg = in.nextInt();

        if(yy <= 0 || mm <= 0 || gg <= 0) //CONTROLLO DATA INSERITA
        {
            System.out.println("errore data non valida");
        }
        else {

            String IT = LocalDate.now().withDayOfMonth(gg).withMonth(mm).withYear(yy).format(italian);
            String EN = LocalDate.now().withDayOfMonth(gg).withMonth(mm).withYear(yy).format(english);
            String FR = LocalDate.now().withDayOfMonth(gg).withMonth(mm).withYear(yy).format(french);
            String JP = LocalDate.now().withDayOfMonth(gg).withMonth(mm).withYear(yy).format(japanese);

            System.out.println("Data italiana: " + IT);
            System.out.println("Data inglese: " + EN);
            System.out.println("Data francese: " + FR);
            System.out.println("Data giapponese: " + JP);

            System.out.println();
            System.out.println("INSERIRE SECONDA DATA");

            System.out.println("Inserire anno: ");
            int yy1 = in.nextInt();
            System.out.println("Inserire mese: ");
            int mm1 = in.nextInt();
            System.out.println("Inseire giorno: ");
            int gg1 = in.nextInt();

            if(yy1 <= 0 || mm1 <= 0 || gg1 <= 0) //CONTROLLO DATA INSERITA
            {
                System.out.println("errore data non valida");
            }
            else {

                String IT1 = LocalDate.now().withDayOfMonth(gg1).withMonth(mm1).withYear(yy1).format(italian);
                String EN1 = LocalDate.now().withDayOfMonth(gg1).withMonth(mm1).withYear(yy1).format(english);
                String FR1 = LocalDate.now().withDayOfMonth(gg1).withMonth(mm1).withYear(yy1).format(french);
                String JP1 = LocalDate.now().withDayOfMonth(gg1).withMonth(mm1).withYear(yy1).format(japanese);

                System.out.println("Data italiana: " + IT1);
                System.out.println("Data inglese: " + EN1);
                System.out.println("Data francese: " + FR1);
                System.out.println("Data giapponese: " + JP1);

            }

        }

        //CALCOLO TEMPO ESECUZIONE PROGRAMMA

        calcoloTempoEsecuzioneProgramma();


    }



}

